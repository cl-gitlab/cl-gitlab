
(in-package :cl-gitlab)

(cl-interpol:enable-interpol-syntax)

(defclass group (set-item)
  ((name :documentation ""
	 :initarg :name)
   (namespace :documentation ""
	      :initarg :namespace
	      :json-id "path")
   (owner-id :documentation ""
	     :initarg :owner-id
	     :json-id "owner_id")
   (projects :documentation ""
	     :initarg :projects
	     :json-type :projects)
   (description :documentation ""
		:initarg :description))
  (:metaclass standard-json-class))

(defmethod url-item-map ((api api-access) (object-class (eql 'group)) op
			 &key id &allow-other-keys)
  (case op
    (:create "/groups/")
    (t (with-asserted-context (id)
	 #?"/groups/${id}"))))

(defmethod url-set-map ((api api-access) (object-class (eql 'group))
			&key &allow-other-keys)
  "/groups")



(defclass group-member (user)
  ((user-id :documentation ""
	    :initarg :user-id
	    :json-id "user_id")
   (access-level :documentation ""
		 :initarg :access-level
		 :json-id "access_level"
		 :json-type :access-level))
  (:metaclass standard-json-class))

(defmethod url-item-map ((api api-access) (object-class (eql 'group-member)) op
			 &key id group &allow-other-keys)
  (with-asserted-context (group)
    (case op
      (:create #?"/groups/${group}/members/")
      (t (with-asserted-context (id)
	   #?"/groups/${group}/members/${id}")))))

(defmethod url-set-map ((api api-access) (object-class (eql 'group-member))
			&key group &allow-other-keys)
  (with-asserted-context (group)
    #?"/groups/${group}/members/"))


(cl-interpol:disable-interpol-syntax)