
(in-package :cl-gitlab)

(cl-interpol:enable-interpol-syntax)

(defclass user (set-item)
  ((email :documentation ""
	  :initarg :email)
   (password :documentation ""
	     :initarg :password)
   (username :documentation ""
	     :initarg :username)
   (name :documentation ""
	 :initarg :name)
   (state :documentation ""
	  :initarg :state)
   (created-at :documentation ""
	       :initarg :created-at
	       :json-id "created_at")
   (skype :documentation ""
	  :initarg :skype)
   (linkedin :documentation ""
	     :initarg :linkedin)
   (twitter :documentation ""
	    :initarg :twitter)
   (website-url :documentation ""
		:initarg :website-url
		:json-id "website_url")
   (avatar-url :documentation ""
	       :initarg :avatar-url
	       :json-id "avatar_url")
   (projects-limit :documentation ""
		   :initarg :projects-limit
		   :json-id "projects_limit")
   (extern-uid :documentation ""
	       :initarg :extern-uid
	       :json-id "extern_uid")
   (provider :documentation ""
	     :initarg :provider)
   (bio :documentation ""
	:initarg :bio)
   (is-admin :documentation ""
	     :initarg :is-admin
	     :json-id "is_admin"
	     :json-type :boolean)
   (theme-id :documentation ""
	     :initarg :theme-id
	     :json-id "theme_id")
   (color-scheme-id :documentation ""
		    :initarg :color-scheme-id
		    :json-id "color_scheme_id")
   (can-create-group :documentation ""
		     :initarg :can-create-group
		     :json-id "can_create_group"
		     :json-type :boolean)
   (can-create-project :documentation ""
		       :initarg :can-create-project
		       :json-id "can_create_project"
		       :json-type :boolean))
  (:metaclass standard-json-class))


(defmethod url-item-map ((api api-access) (object-class (eql 'user)) op
			 &key id &allow-other-keys)
  (case op
    (:create "/users")
    (t (with-asserted-context (id)
	 #?"/users/${id}"))))

(defmethod url-set-map ((api api-access) (object-class (eql 'user))
			&key &allow-other-keys)
  "/users")



(defclass session-user (user)
  ((login :documentation ""
	  :initarg :login))
  (:metaclass standard-json-class))

(defmethod url-item-map ((api api-access)
			 (object-class (eql 'session-user))
			 op &key &allow-other-keys)
  (case op
    (:create "/session")
    (t "/user")))

(defmethod url-set-map ((api api-access)
			(object-class (eql 'session-user))
			&key &allow-other-keys)
  (error "operation not supported: no set for objects of type 'current user'"))

(defmethod create-item ((api api-access) (object session-user) &rest context)
  (error "operation not supported: can't create objects of type 'current user'"))

(defmethod delete-item ((api api-access) (object session-user) &rest context)
  (error "operation not supported: can't delete current user."))




(defclass user-ssh-key (set-item)
  ((title :documentation ""
	  :initarg :title)
   (key :documentation ""
	:initarg :key)
   (created-at :documentation ""
	       :initarg :created-at
	       :json-id "created_at"))
  (:metaclass standard-json-class))


(defmethod url-item-map ((api api-access) (object-class (eql 'user-ssh-key)) op
			 &key id user &allow-other-keys)
  (assert user (user) "user")
  (let ((user-path (url-item-map api user :retrieve)))
    (case op
      (:create #?"${user-path}/keys")
      (t (with-asserted-context (id)
	   #?"${user-path}/keys/${id}")))))

(defmethod url-set-map ((api api-access) (object-class (eql 'user-ssh-key))
			&key user &allow-other-keys)
  (assert user (user) "user")
  (let ((user-path (url-item-map api user :retrieve)))
    #?"${user-path}/keys/"))


(cl-interpol:disable-interpol-syntax)