cl-gitlab
=========

API for running gitlab on common-lisp.net

## Features

 * Users (with their SSH keys management)
 * Groups (with their members management)
 * Rudimentary projects support


## Changes
 * Much more docs
 * Support for the API object SSH-keys (both 'regular' and 'current' users)
 * Lots of code reorg
 * More supported fields in the 'group' class
 * Start to support the 'project' class

