
(in-package :cl-gitlab)


;;; Generics and default mapping for `:booleans'

(defgeneric json-type-encode-fn (json-type)
  (:documentation "Returns a function of one argument which is used
to pre-process the value to be encoded to JSON.")
  (:method (json-type) #'identity)
  (:method ((json-type (eql :boolean)))
    #'(lambda (value)
	(if value 'yason:true 'yason:false))))

(defgeneric json-type-decode-fn (json-type)
  (:documentation "Returns a function of one argument which is used
to map the value read from JSON.")
  (:method (json-type) #'identity)
  (:method ((json-type (eql :boolean)))
    #'(lambda (value)
	(unless (eq value 'yason:false)
	  value))))


;;; Mapping for `:access-level', an enum type

(defmethod json-type-encode-fn ((json-type (eql :access-level)))
  #'(lambda (value)
      (ecase value
	(:guest     10)
	(:reporter  20)
	(:developer 30)
	(:master    40)
	(:owner     50))))

(defmethod json-type-decode-fn ((json-type (eql :access-level)))
  #'(lambda (value)
      (ecase value
	(10 :guest)
	(20 :reporter)
	(30 :developer)
	(40 :master)
	(50 :owner))))

;;; Mapping for `:visibility-level', an enum type

(defmethod json-type-encode-fn ((json-type (eql :visibility-level)))
  #'(lambda (value)
      (ecase value
	(:private    0)
	(:internal  10)
	(:public    20))))

(defmethod json-type-decode-fn ((json-type (eql :visibility-level)))
  #'(lambda (value)
      (ecase value
	( 0 :private)
	(10 :internal)
	(20 :public))))


;;; Mapping for `:projects' (a collection of objects of class PROJECT)

(defmethod json-type-decode-fn ((json-type (eql :projects)))
  #'(lambda (value)
      (mapcar #'(lambda (yr)
		  (instance-from-response 'project yr))
	      value)))

(defmethod json-type-encode-fn ((json-type (eql :projects)))
  ;; This is a collection; it can't be encoded --> return no function
  nil)


