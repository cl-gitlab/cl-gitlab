
(in-package :cl-gitlab)

;;; TODO

;; * Create a `dispatch' which allows retrieval of prev/next set URLs
;; * rename api-access -> api-session
;; * Change result type for retrieve-set
;; * Implement mapper function for set-page and result-set types


(defclass server-access ()
  ((url :documentation "The base API url, e.g. http://example.com/api/v3"
	:accessor server-access-url
	:initarg :url))
  (:documentation "Properties of the server being accessed."))

(defclass api-access ()
  ((server :documentation
	   "A `server-access' object for the connected server."
	   :accessor api-access-server
	   :initarg :server)
   (user :documentation "A `current-user' object for the connected user."
	 :accessor api-access-user
	 :initarg :user)
   (token :documentation
	  "The token returned by the server at session initialization."
	  :accessor api-access-token
	  :initarg :token)
   (cached-stream :documentation
		  "A stream to be re-used from a previous api (HTTP) request."
		  :accessor api-access-cached-stream
		  :initform nil))
  (:documentation "Container for all API session related values."))

(defclass set-item (standard-json-object)
  ((id :documentation "Unique identifier of the object instance."
       :accessor item-id
       :initarg :id
       :json-id "id"))
  (:documentation "Base class for all objects defined in the API.

Uses the STANDARD-JSON-CLASS metaclass to use slots with JSON fields,
see `json-class-slot-mixin' for more.")
  (:metaclass standard-json-class))

(defclass set-page ()
  ((items :documentation ""
	  :initarg :items
	  :accessor items)
   (first :documentation ""
	  :initarg :first)
   (last :documentation ""
	 :initarg :last)
   (next :documentation ""
	 :initarg :next)
   (prev :documentation ""
	 :initarg :prev)))

(defgeneric has-next-p (set-page)
  (:documentation ""))

(defgeneric has-prev-p (set-page)
  (:documentation ""))

(defmethod has-next-p ((set-page set-page))
  (and (slot-boundp set-page 'next)
       (slot-value set-page 'next)))

(defmethod has-prev-p ((set-page set-page))
  (and (slot-boundp set-page 'prev)
       (slot-value set-page 'prev)))



(defgeneric serialize (object)
  (:documentation "Function to serialize `object' into JSON form."))

(defgeneric create-item (api-access object &rest context)
  (:documentation "Function to create a new object of the type of the
object passed in, with the values taken from `object'.

In case the object type is associated with another object, that associated
object should be passed in as a keyword argument, e.g. when an ssh key
for a specific user is being created, the associated user object should
be passed in using

   (create-item <api-access> <ssh-object> :user <user-object>)

"))

(defgeneric delete-item (api-access object &rest context)
  (:documentation "Function to delete an object of the type of the
object passed in. The role of the `context' is explained in the
documentation for `create-item'."))

(defgeneric update-item (api-access object &rest context)
  (:documentation "Function to update an object of the type of the
object passed in, with the values to be updated to taken from `object'.

The role of the `context' is explained in the documentation
for `create-item'."))

(defgeneric retrieve-set (api-access object-class &rest context 
				     &key force &allow-other-keys)
  (:documentation "Retrieve a set of objects of the indicated class.
#### We need to indicate a return type here!!

In case the object type is associated with another object, that associated
object should be passed in as a keyword argument as explained in the
documentation for `create-item'"))

(defgeneric retrieve-item (api-access object-class &rest context
				      &key id force &allow-other-keys)
  (:documentation "Retrieves the item of class `object-classs' indicated
by `id' using the `context' as described in the documentation for `create-item'.

Not all classes require the `id' keyword, e.g. `current-user' which has
uniqueness implied in the type.
"))

(defgeneric url-item-map (api-access object-or-class-name op &rest context
				     &key id &allow-other-keys)
  (:documentation "Returns the path to be used for `op' (a symbol, e.g.
`:create', `:update', `:delete') on the object or class-name. The `context'
is the same as documented in `create-item'.")
  (:method (api-access object-or-class-name op &rest context)
    (error "url-item-map must be specialized")))

(defgeneric url-set-map (api-access object-or-class-name &rest context
				    &key &allow-other-keys)
  (:documentation "Returns the path to be used for access of the set
of the type of object or class-name passed in. The `context' is the same
as documented in `create-item'.")
  (:method (api-access object-or-classname &rest context)
    (error "url-set-map must be specialized.")))


(defun dispatch (api-access http-method api-url request-body
		 &key (json-object-as :plist))
  "Low level function to dispatch HTTP requests for API access
with error handling and connection re-use."
  (let ((full-url (concatenate 'string
			       (server-access-url
				(api-access-server api-access))
			       api-url))
	(stream (api-access-cached-stream api-access))
	(headers (when (slot-boundp api-access 'token) 
		   `(("PRIVATE-TOKEN" . ,(api-access-token api-access)))))) 
    (setf (api-access-cached-stream api-access) nil)
    (multiple-value-bind
	  (body-or-stream status-code headers uri stream must-close
			  reason-phrase)
	(drakma:http-request full-url
			     :method http-method
			     :content request-body
			     :content-type "application/json; charset=utf-8"
			     :want-stream t
			     :keep-alive t
			     :close nil
			     :stream stream
			     :additional-headers headers
			     :external-format-out :utf-8
			     :external-format-in :utf-8)
      (declare (ignore body-or-stream uri reason-phrase))
      (multiple-value-bind
	    (type subtype parameters)
	  (drakma:get-content-type headers)
	(declare (ignore parameters)) 
	(unless (and (string= type "application")
		     (string= subtype "json"))
	  (close stream)
	  (error 'content-type-error)))
      ;; we have a meaningful content type -- however, that doesn't
      ;;    mean we have a meaningful response
      (unwind-protect
	   (let ((response (yason:parse stream :object-as json-object-as)))
	     (unless must-close
	       (setf (api-access-cached-stream api-access) stream))
	     (unless (status-code-class-ok status-code)
	       (error 'api-error :response response
		      :status-code status-code))
	     (values response status-code))
	(when must-close
	  (close stream))))))

(defun class-initarg-from-key (class key)
  "Finds the initarg for `class' to use for the JSON `key' identifier."
  (when (symbolp class)
    (setf class (find-class class)))
  (let ((slot (find key (class-slots class)
		    :key #'slot-json-id
		    :test #'string=)))
    (when slot
      (first (slot-definition-initargs slot)))))

(defun instance-from-response (class yason-result)
  "Create an instance of type `class' with its slots set based on the JSON
response `yason-result'."
  (when (symbolp class)
    (setf class (find-class class))) 
  (unless (class-finalized-p class)
    (finalize-inheritance class))
  (apply 'make-instance
	 class
	 (if (hash-table-p yason-result)
	     (loop for key being each hash-key of yason-result
		using (hash-value value)
		for initarg = (class-initarg-from-key class key)
		when initarg
		nconc (list initarg value))
	     (loop for (key value) on yason-result by #'cddr
		for initarg = (class-initarg-from-key class key)
		when initarg
		nconc (list initarg value)))))

(defmacro with-asserted-context (variables &body body)
  "Asserts that required context variables are non-nil and assigned
atom values (i.e. not objects).  Converts objects (non-atoms) to atoms
by evaluating '(setf <variable> (item-id <variable))'."
  `(progn
     ,@(loop for var in variables
	    collect `(progn
		       (assert ,var
			       (,var)
			       "Missing context: no ~s specified.")
		       (when (typep ,var 'set-item)
			 (setf ,var
			       (item-id ,var)))
		       ,@body))))



(defmethod serialize ((object set-item))
  (serialize-json-object object))

(defmethod create-item (api-access object &rest context)
  (let* ((api-url (apply #'url-item-map api-access object :create context))
	 (request-body (serialize object))
	 (yason-result (dispatch api-access :POST api-url request-body)))
    (instance-from-response (class-of object) yason-result)))

(defmethod update-item (api-access object &rest context)
  (let* ((api-url (apply #'url-item-map api-access object :update context))
	 (request-body (serialize object))
	 (yason-result (dispatch api-access :PUT api-url request-body)))
    (when yason-result
      (instance-from-response object-class yason-result))))

(defmethod delete-item (api-access object &rest context)
  (let* ((api-url (apply #'url-item-map api-access object :delete context))
	 (yason-result (dispatch api-access :DELETE api-url "")))
    (when yason-result
      (instance-from-response (class-of object) yason-result))))

(defmethod retrieve-item (api-access object-class &rest context)
  (let* ((api-url (apply #'url-item-map api-access
			 object-class :retrieve context))
	 (yason-result (dispatch api-access :GET api-url "")))
    (when yason-result
      (instance-from-response object-class yason-result))))

(defmethod retrieve-set (api-access object-class &rest context)
  (let* ((api-url (apply #'url-set-map api-access object-class context))
	 (yason-result (dispatch api-access :GET api-url "")))
    (when (listp yason-result)
      (mapcar #'(lambda (yr)
		  (instance-from-response object-class yr))
	      yason-result))))

(defmethod url-item-map (api-access (object set-item) op &rest keys)
  (unless (getf keys :id)
    (when (slot-boundp object 'id) 
      (setf keys (list* :id (item-id object) keys))))
  (apply #'url-item-map api-access (class-name (class-of object))
	 op keys))

(defmethod url-set-map (api-access (object set-item) &rest keys)
  (apply #'url-set-map api-access (class-name (class-of object)) keys))

(defun status-code-class-informational (status-code)
  (<= 0 status-code 199))

(defun status-code-class-ok (status-code)
  (<= 200 status-code 299))

(defun status-code-class-redirect (status-code)
  (<= 300 status-code 399))

(defun status-code-class-client-error (status-code)
  (<= 400 status-code 499))

(defun status-code-class-server-error (status-code)
  (<= 500 status-code 599))

(defun status-code-class-error (status-code)
  (<= 400 status-code))

(defun login (&key username email password base-url)
  "Create an `api-session' against a server indicated by `base-url',
using one of `username' and `email', authenticating using `password'.

Returns two values: (api-access status-code). `api-access' is nil on failure.

`base-url' would usually be of the form 'https://<server>/api/v3/'."
  (let* ((session-user (if username
			   (make-instance 'session-user
					  :login username
					  :password password)
			   (make-instance 'session-user
					  :email email
					  :password password)))
	 (server-access (make-instance 'server-access :url base-url))
	 (api-access (make-instance 'api-access :server server-access))
	 (api-url (apply #'url-item-map api-access session-user :create nil))
	 (request-body (serialize session-user)))
    (multiple-value-bind
	  (yason-result status-code)
	(dispatch api-access :post api-url request-body
		  :json-object-as :hash-table)
      (cond
	((eql 201 status-code)
	 (let ((token (gethash "private_token" yason-result))
	       (api-user (instance-from-response 'session-user yason-result)))
	   (setf (api-access-user api-access) api-user)
	   (setf (api-access-token api-access) token)
	   (values api-access 201)))
	(t
	 (logout api-access)
	 (values nil status-code))))))


(defun logout (api-access)
  "Clean up and close an existing API session."
  (when (api-access-cached-stream api-access)
    (ignore-errors (close (api-access-cached-stream api-access)))))
