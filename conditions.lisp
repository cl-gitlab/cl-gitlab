
(in-package :cl-gitlab)

(define-condition cl-gitlab-error (error)
  ())

(define-condition content-type-error (cl-gitlab-error)
  ()
  (:documentation "Signalled if the content type of the server response
indicates content other than JSON."))


(defun print-api-error (the-error stream)
  (format stream "GitLab api returned error ~A with response ~S"
	  (api-error-status-code the-error)
	  (api-error-response the-error)))


(define-condition api-error (cl-gitlab-error)
  ((response :initarg :response
	     :reader api-error-response)
   (status-code :initarg :status-code
		:reader api-error-status-code))
  (:documentation "Signalled if the remote API invocation returns anything
other than a 2xx response.")
  (:report print-api-error))

