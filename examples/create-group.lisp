
;; requires cl-gitlab to be loaded

(in-package :cl-user)

(defvar *session*)

;; First: set up a session with the remote
(setf *session* 
      (cl-gitlab:login :url "http://common-lisp.net.example.com/api/v3"
		       :username "someuser"
		       ;; the above line could be replaced with
		       ;; :email "someuser-at-common-lisp.net@example.net"
		       :password "the-super-secret"))

;; Now we can "do stuff"

(defvar *created-group*
  (cl-gitlab:create-item
   *session*
   (make-instance 'cl-gitlab:group
		  :name "some-new-group"
		  :namespace "newestgroup")))

;; add users to the group

(cl-gitlab:create-item
 *session*
 (make-instance 'cl-gitlab:group-member :id 15) ;; 15 == ID of the user
 :group *created-group*)



;; Make sure to clean up sockets etc, by logging out

(cl-gitlab:logout *session*)
(setf *session* nil)