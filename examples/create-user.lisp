
;; requires cl-gitlab to be loaded

(in-package :cl-user)

(defvar *session*)

;; First: set up a session with the remote
(setf *session* 
      (cl-gitlab:login :url "http://common-lisp.net.example.com/api/v3"
		       :username "someuser"
		       ;; the above line could be replaced with
		       ;; :email "someuser-at-common-lisp.net@example.net"
		       :password "the-super-secret"))

;; Now we can "do stuff"

(defvar *created-user*
  (cl-gitlab:create-item
   *session*
   (make-instance 'cl-gitlab:user
		  :username "some-new-user"
		  :email "some-new-user-at-common-lisp.net@example.net"
		  :password "some-super-secret")))

;; add ssh keys to the user for repository access

(cl-gitlab:create-item
 *session*
 (make-instance 'cl-gitlab:user-ssh-key
		:title "key 1"
		:key "AAAA....")
 :user *created-user*)


;; Make sure to clean up sockets etc, by logging out

(cl-gitlab:logout *session*)
(setf *session* nil)