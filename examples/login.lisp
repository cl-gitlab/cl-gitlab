
;; requires cl-gitlab to be loaded

(in-package :cl-user)

(defvar *session*)

;; First: set up a session with the remote
(setf *session* 
      (cl-gitlab:login :url "http://common-lisp.net.example.com/api/v3"
		       :username "someuser"
		       ;; the above line could be replaced with
		       ;; :email "someuser-at-common-lisp.net@example.net"
		       :password "the-super-secret"))

;; Now we can "do stuff"

;; ...doing stuff...



;; Make sure to clean up sockets etc, by logging out

(cl-gitlab:logout *session*)
(setf *session* nil)