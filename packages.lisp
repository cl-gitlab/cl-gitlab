

(defpackage #:cl-gitlab
  (:use :closer-common-lisp :drakma :yason :closer-mop)
  (:export

   ;;;; generic api
   #:login
   #:logout

   ;; api-access
   #:item-id

   ;; set-page
   #:has-next-p
   #:has-prev-p

   ;; set-item generics
   #:create-item
   #:delete-item
   #:update-item
   #:retrieve-item
   #:retrieve-set

   ;;;; api classes

   ;; user-related
   #:user
   #:session-user
   #:user-ssh-key

   ;; group-related
   #:group
   #:group-member
   
   ;; project-related
   #:project
   ))
