

(defsystem #:cl-gitlab
  :components ((:file "packages")
	       (:file "conditions" :depends-on ("packages"))
	       (:file "json-types" :depends-on ("packages"))
	       (:file "meta" :depends-on ("packages" "json-types"))
	       (:file "base" :depends-on ("meta" "conditions"))
	       (:file "users" :depends-on ("base"))
	       (:file "projects" :depends-on ("users"))
	       (:file "groups" :depends-on ("base")))
  :depends-on ("drakma" "yason" "closer-mop" "cl-interpol"))
	       
