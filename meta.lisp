
(in-package :cl-gitlab)

(defclass standard-json-class (standard-class)
  ()
  (:documentation "Specialized metaclass for non-standard slot class."))

(defmethod validate-superclass ((class standard-json-class)
				(superclass standard-class))
  t)

(defclass standard-json-object () ()
  (:documentation "Class for JSON object instances.")
  (:metaclass standard-json-class))

(defclass json-class-slot-mixin ()
  ((json-id :accessor slot-json-id
	    :initarg :json-id
	    :documentation "Key or name in the JSON object by which to
identify the value of the slot.")
   (json-type :accessor slot-json-type
	      :initarg :json-type
	      :initform nil
	      :documentation "JSON type of the slot's value to aid mapping;
e.g. the value `NIL` could be mapped to JSON values `null` or `false`.")
   (json-required-operations :accessor slot-json-required-operations
			     :initarg :json-required-operations
			     :initform nil
			     :documentation "A list of operations
for which this field is required; :create, :update, :retrieve and :delete.")))

(defclass json-class-direct-slot-definition (json-class-slot-mixin
					     standard-direct-slot-definition)
  ())

(defclass json-class-effective-slot-definition (json-class-slot-mixin
						standard-effective-slot-definition)
  ())

(defmethod initialize-instance :after
    ((instance json-class-direct-slot-definition) &rest initargs)
  ;; the effective slot definition class gets initialized in
  ;; compute-effective-slot-definition
  (declare (ignore initargs))
  (unless (slot-boundp instance 'json-id)
    (setf (slot-json-id instance)
	  (string-downcase (string (slot-definition-name instance))))))

(defmethod direct-slot-definition-class ((class standard-json-class)
					 &rest initargs)
  (declare (ignorable initargs))
  (find-class 'json-class-direct-slot-definition))

(defmethod effective-slot-definition-class ((class standard-json-class)
					    &rest initargs)
  (declare (ignorable initargs))
  (find-class 'json-class-effective-slot-definition))

(defmethod compute-effective-slot-definition ((class standard-json-class)
					      name direct-slot-definitions)
  (let ((effective-slot-definition (call-next-method))
	(direct-slot-definition (car direct-slot-definitions)))
 ;;   (print direct-slot-definition)
 ;;   (describe direct-slot-definition)
    (when direct-slot-definition
      (dolist (slot-name '(json-id json-type json-required-operations))
	(setf (slot-value effective-slot-definition slot-name)
	      (slot-value direct-slot-definition slot-name))))
    effective-slot-definition))




(defun serialize-json-object (object)
  (yason:with-output-to-string* ()
      (yason:with-object ()
	(mapcar #'(lambda (slot)
		    (let* ((slot-name (slot-definition-name slot))
			   (output-key (slot-json-id slot))
			   (encode-fn (json-type-encode-fn
					 (slot-json-type slot)))
			   (value (when (and output-key
					     (slot-boundp object slot-name))
				    (slot-value object slot-name))))
		      (when (and value encode-fn)
			(yason:encode-object-element output-key
						     (funcall encode-fn
							      value)))))
		(class-slots (class-of object))))))
