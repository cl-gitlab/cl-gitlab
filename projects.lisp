(in-package :cl-gitlab)

(cl-interpol:enable-interpol-syntax)

(defclass project (set-item)
  ()
  (:metaclass standard-json-class))

(cl-interpol:disable-interpol-syntax)
